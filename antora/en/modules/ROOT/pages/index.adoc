= A scientific anniversary for each day of the year.

The Scientific School Calendar is aimed mainly at students in primary and secondary education. Every day a scientific or technological anniversary has been collected, such as, for example, births of people or commemorations of remarkable finds.

The calendar is accompanied by a didactic guide with guidelines for its transversal educational use in classes. These didactic proposals are based on the principles of inclusion, standardization and equity and are accompanied by general accessibility guidelines. For this, varied tasks are provided that include a wide range of skills and levels of difficulty and that, developed cooperatively, allow all students to make useful and relevant contributions.

To promote its dissemination and use in the classroom, all the materials are translated into several languages: Spanish, Galician, Basque, Catalan, Asturian, Aragonese, English, French, Esperanto and Arabic.

The information with the daily anniversaries and the accompanying illustrations are freely available in this repository. In addition, the calendar and guide (layout in pdf and plain text) can be downloaded from the IGM website (http://www.igm.ule-csic.es/calendario-cientifico).

This initiative aims to bring scientific culture closer to the younger population and to create the closest possible references for them. For this reason, a greater effort has been made to show people and findings of the present that constitute references for young people and, at the same time, give a vision of dynamism and timeliness. Special attention has been paid to promoting a non-sexist language and increasing the visibility of women scientists and technologists, in order to make available reference models that promote scientific-technical vocations among girls and adolescents. 

== Twitter

@CalCientifico

== Telegram

https://t.me/CalendarioCientifico

== iCal

link:{attachmentsdir}/en.ical[Download]
