= Urteurren zientifiko bat urtearen egun bakoitzerako.

Eskola egutegi zientifikoa lehen hezkuntzako eta bigarren hezkuntzako ikasleei bereziki bideraturik dago.Egun bakoitzean urteurren zientifiko edo teknologiko bat erantsi da, arlo horietako persona baten jaiotza edo aurkikuntza aipagarri baten urteurrena adibidez.

Aipaturiko egutegiarekin batera hezkuntzaren erabilpen transbertsalaren inguruan orientazioa ematea xede duen guida didaktiko bat txertatzen da. Proposamen didaktiko hauek inkusio,normalizazio eta equitate printzioak dituzte oinarri eta eskuragarritasun jarraibideekin batera datoz. Horretarako, trebetasun eta zailtasunmaila ugari dituzten eta ikasle guztiek elkarlanean garatuta, ekarpen baliagarriak eta garrantzitsuak egiten ahalbidetzen duten askotariko zereginak eskaintzen dira.

Eduki honen hedapena eta erabilpena sustatzeko, material osoa hainbat hizkutzatara itzulita egongo da: gaztelania, galiziera, euskara, katalana,asturianoa, aragoiera, ingelesa, frantsesa, esperantoa eta arabiera.

Eguneko urteurrenen informazioa eta datozen ilustrazioak biltegi honetan irekian eskuragarri egongo dira. Gainera egutegia eta guida(maketatua pdf-n eta testu lauan) IGMaren webgunean jaitsi daitezke(http://www.igm.ule-csic.es/calendario-cientifico).

Egitasmo honen helburu nagusia kulltura zientifikoa biztanleria gazteari hurbiltzea eta haientzat ahalik eta hurbilen dauden erreferenteak sortzea da. Horren ondorioz gazteentzat erreferentzia diren eta aldi berean dinamismoaren eta gaurkotasunaren ikuspegia daramaten, gaur eguneko pertsona eta aurkikuntzak ezaguztarazten esfortzu handiagoa egin da. Haurren eta nerabeen artean bokazio zientifiko-teknikoak sustatuko dituzten eredu erreferenteak eskura jartzeko, hizkuntza ez-sexista sustatzeari eta emakume zientifikoen eta teknologoen ikusgaitasuna areagotzeari arreta berezia eskaini zaio. Honetaz gain espainiar zentro publikoen ikerkuntza jarduera dibulgatzean enfasia jarri da.

== Twitter

@CalCientifico

== Telegram

https://t.me/CalendarioCientifico

== iCal

link:{attachmentsdir}/eus.ical[Download]
